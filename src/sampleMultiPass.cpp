#include "sampleMultiPass.h"

static const glm::vec3 COLOR_CHOICES[] = {
     glm::vec3(1.0f, 0.0f, 0.0f),
     glm::vec3(1.0f, 1.0f, 0.0f),
     glm::vec3(0.0f, 1.0f, 0.0f),
     glm::vec3(0.0f, 0.0f, 1.0f),
     glm::vec3(0.0f, 1.0f, 1.0f),
     glm::vec3(1.0f, 0.0f, 1.0f),
     glm::vec3(1.0f, 0.5f, 0.25f),
     glm::vec3(0.25f, 1.0f, 0.5f),
};

static const int NUM_COLOR_CHOICES = sizeof(COLOR_CHOICES) / sizeof(COLOR_CHOICES[0]);

SampleMultiPass::~SampleMultiPass()
{
	printf("Destroying Multi Pass Lights Sample\n");
    wolf::MaterialManager::DestroyMaterial(m_pMatLights);
    wolf::MaterialManager::DestroyMaterial(m_pMatBase);
    wolf::TextureManager::DestroyTexture(m_pTex);
    wolf::TextureManager::DestroyTexture(m_pNormalMap);
    delete m_pModel;
    delete m_pGrid;
    delete m_pOrbitCam;
    for(int i = 0; i < m_lights.size(); ++i)
        delete m_lights[i].pDbgSphere;
}

void SampleMultiPass::_addLight()
{
    Light light;

    // Choose random color 
    light.color = COLOR_CHOICES[rand() % NUM_COLOR_CHOICES];
    light.position = glm::vec3(wolf::randFloat(-20.0f, 20.0f),
                               wolf::randFloat(2.0f, 20.0f),
                               wolf::randFloat(-20.0f, 20.0f));
    light.dest = glm::vec3(wolf::randFloat(-20.0f, 20.0f),
                           wolf::randFloat( 2.0f, 20.0f),
                           wolf::randFloat(-20.0f, 20.0f));
    light.pDbgSphere = new Sphere(0.25f);
    light.pDbgSphere->SetColor(light.color);

    m_lights.push_back(light);
}

void SampleMultiPass::init()
{
	// Only init if not already done
    if(!m_pModel)
    {
        m_pTex = wolf::TextureManager::CreateTexture("data/diffuseGray.png");
        m_pTex->SetWrapMode(wolf::Texture::WM_Repeat);

        m_pNormalMap = wolf::TextureManager::CreateTexture("data/normalMap.png");
        m_pNormalMap->SetWrapMode(wolf::Texture::WM_Repeat);

        m_pMatLights = wolf::MaterialManager::CreateMaterial("sampleMultiPass_Lights");
        m_pMatLights->SetProgram("data/fourPointLights_add.vsh", "data/fourPointLights_add.fsh");
        m_pMatLights->SetDepthTest(true);
        m_pMatLights->SetDepthWrite(true);
        m_pMatLights->SetDepthFunc(wolf::DF_LessEqual);
        m_pMatLights->SetBlend(true);
        m_pMatLights->SetBlendMode(wolf::BM_One, wolf::BM_One);
        m_pMatLights->SetUniform("u_ambientLight", glm::vec3(0.2f,0.2f,0.2f));
        m_pMatLights->SetUniform("u_specularColor", glm::vec3(1.0f,1.0f,1.0f));
        m_pMatLights->SetUniform("u_shininess", 200.0f);
        m_pMatLights->SetTexture("u_diffuseTex", m_pTex);
        m_pMatLights->SetTexture("u_normalMap", m_pNormalMap);

        m_pMatBase = wolf::MaterialManager::CreateMaterial("sampleMultiPass_Base");
        m_pMatBase->SetProgram("data/lightBase.vsh", "data/lightBase.fsh");
        m_pMatBase->SetDepthTest(true);
        m_pMatBase->SetDepthWrite(true);
        m_pMatBase->SetBlend(false);
        m_pMatBase->SetUniform("u_ambientLight", glm::vec3(0.2f,0.2f,0.2f));
        m_pMatBase->SetUniform("u_specularColor", glm::vec3(1.0f,1.0f,1.0f));
        m_pMatBase->SetUniform("u_shininess", 200.0f);
        m_pMatBase->SetTexture("u_diffuseTex", m_pTex);
        m_pMatBase->SetTexture("u_normalMap", m_pNormalMap);

        // Add initial light
        _addLight();

        m_pModel = new wolf::Model("data/unrealSmall.fbx");

        glm::vec3 min = m_pModel->getAABBMin();
        glm::vec3 max = m_pModel->getAABBMax();
        glm::vec3 center = m_pModel->getAABBCenter();

        m_pOrbitCam = new OrbitCamera(m_pApp);
        m_pOrbitCam->focusOn(min,max);

        float gridSize = 2.5f * wolf::max(max.x,max.z);
        m_pGrid = new Grid3D(10, gridSize / 10.0f);
        m_pGrid->hideAxes();
    }

    printf("Successfully initialized Multi Pass Lights Sample\n");
}

void SampleMultiPass::update(float dt) 
{
    m_pOrbitCam->update(dt);
    m_pGrid->update(dt);

    m_timer += dt;

    for(unsigned int i = 0; i < m_lights.size(); ++i)
    {
        Light& light = m_lights[i];

        glm::vec3 delta = light.dest - light.position;
        float dist = glm::length(delta);

        if(dist <= 0.5f)
        {
            light.dest = glm::vec3(wolf::randFloat(-20.0f, 20.0f),
                                   wolf::randFloat(2.0f, 25.0f),
                                   wolf::randFloat(-20.0f, 20.0f));
        }
        else 
        {
            glm::vec3 dir = glm::normalize(delta);
            light.position += dir * 4.5f * dt;
        }

        light.pDbgSphere->SetPosition(light.position);
    }

    if(m_pApp->isKeyDown('l'))
        m_keyDown = true;
    else 
    {
        if(m_keyDown)
            _addLight();

        m_keyDown = false;
    }
}

void SampleMultiPass::render(int width, int height)
{
	glClearColor(0.3f, 0.3f, 0.3f, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 mProj = m_pOrbitCam->getProjMatrix(width, height);
	glm::mat4 mView = m_pOrbitCam->getViewMatrix();
	glm::mat4 mWorld = glm::rotate(glm::mat4(1.0f), m_rot, glm::vec3(0.0f, 1.0f, 0.0f));

    m_pGrid->render(mView, mProj);

    m_pMatLights->SetUniform("u_viewPos", m_pOrbitCam->getViewPosition());

    m_pTex->Bind();

    for(unsigned int i = 0; i < m_lights.size(); ++i)
    {
        m_lights[i].pDbgSphere->Render(mWorld,mView,mProj);
    }

    m_pModel->OverrideMaterial(m_pMatBase);
    m_pModel->Render(mWorld, mView, mProj);

    unsigned int lightsLeftToRender = m_lights.size();

    m_pModel->OverrideMaterial(m_pMatLights);

    int sceneLightIdx = 0;
    while(lightsLeftToRender > 0)
    {
        // NOTE: Don't do per-frame string operations like this when possible - it's not efficient.
        //       I'm only doing it to keep the code concise for sample purposes
        char buff[64];

        int numThisPass = lightsLeftToRender > NUM_LIGHTS_PER_PASS ? NUM_LIGHTS_PER_PASS : lightsLeftToRender;

        for (int i = 0; i < NUM_LIGHTS_PER_PASS; ++i)
        {
            glm::vec4 posRange = glm::vec4(glm::vec3(0.0f,0.0f,0.0f), 100.0f);
            glm::vec3 color = glm::vec3(0.0f,0.0f,0.0f);

            if(i < numThisPass)
            {
                const Light& light = m_lights[sceneLightIdx];
                posRange = glm::vec4(light.position, 100.0f);
                color = light.color;
            }

            sprintf(buff, "u_light%dPosRange", i + 1);
            m_pMatLights->SetUniform(buff, posRange);

            sprintf(buff, "u_light%dColor", i + 1);
            m_pMatLights->SetUniform(buff, color);

            sprintf(buff, "u_light%dAttenuation", i + 1);
            m_pMatLights->SetUniform(buff, glm::vec3(0.0f, 0.3f, 0.0f));

            sceneLightIdx++;
        }

        m_pModel->Render(mWorld, mView, mProj);

        lightsLeftToRender -= numThisPass;
    }
}

