#include "sampleMultiSpotLights.h"

static const glm::vec3 LIGHT_POSITIONS[] = {
    glm::vec3(10.0f,2.0f,10.0f),
    glm::vec3(-10.0f,2.0f,10.0f),
    glm::vec3(10.0f,2.0f,-10.0f),
    glm::vec3(-10.0f,2.0f,-10.0f)
};

static const glm::vec3 LIGHT_COLORS[] = {
    glm::vec3(1.0f,0.0f,0.0f),
    glm::vec3(0.0f,1.0f,0.0f),
    glm::vec3(0.0f,0.0f,1.0f),
    glm::vec3(1.0f,1.0f,0.0f)
};

class SingleMaterialProvider : public wolf::Model::MaterialProvider
{
public:
    SingleMaterialProvider(const std::string& matName) : m_matName(matName) { }

    wolf::Material* getMaterial(const std::string& nodeName, int subMeshIndex, const std::string& name) const override
    {
        // Regardless of what mesh index or mat name the model wants, we just
        // use the mat we were seeded with. Note that we create a new one each
        // time so the DestroyMaterial calls line up. This could be improved,
        // but they do share shaders.
        return wolf::MaterialManager::CreateMaterial(m_matName);
    }

private:
    std::string m_matName;
};

SampleMultiSpotLights::~SampleMultiSpotLights()
{
	printf("Destroying Multi Spot Light Sample\n");
    if(m_pModel)
    {
        wolf::MaterialManager::DestroyMaterial(m_pMat);
        wolf::TextureManager::DestroyTexture(m_pTex);
        delete m_pModel;
        delete m_pGrid;
        delete m_pOrbitCam;
        for (int i = 0; i < 4; ++i)
            delete m_pLightDbgSpheres[i];
    }
}

void SampleMultiSpotLights::init()
{
	// Only init if not already done
    if(!m_pModel)
    {
        m_pTex = wolf::TextureManager::CreateTexture("data/diffuseGray.png");
        m_pTex->SetWrapMode(wolf::Texture::WM_Repeat);

        const std::string MATNAME = "sampleMultiSpotLights";
        m_pMat = wolf::MaterialManager::CreateMaterial(MATNAME);
        m_pMat->SetProgram("data/fourSpotLights.vsh", "data/fourSpotLights.fsh");
        m_pMat->SetDepthTest(true);
        m_pMat->SetDepthWrite(true);

        SingleMaterialProvider matProvider(MATNAME);
        m_pModel = new wolf::Model("data/unrealSmall.fbx", matProvider);

        glm::vec3 min = m_pModel->getAABBMin();
        glm::vec3 max = m_pModel->getAABBMax();
        m_modelCenter = m_pModel->getAABBCenter();

        char buff[64];
        for(int i = 0; i < 4; ++i)
        {
            m_pLightDbgSpheres[i] = new Sphere(0.25f);
            m_pLightDbgSpheres[i]->SetPosition(LIGHT_POSITIONS[i]);
            m_pLightDbgSpheres[i]->SetColor(LIGHT_COLORS[i]);

            sprintf(buff, "u_light%dPosRange", i + 1);
            m_pMat->SetUniform(buff, glm::vec4(LIGHT_POSITIONS[i],100.0f));

            sprintf(buff, "u_light%dColor", i + 1);
            m_pMat->SetUniform(buff, LIGHT_COLORS[i]);

            sprintf(buff, "u_light%dAttenuation", i + 1);
            m_pMat->SetUniform(buff, glm::vec3(0.0f,0.3f,0.0f));

            glm::vec3 spotDir = glm::normalize(m_modelCenter - LIGHT_POSITIONS[i]);
            sprintf(buff, "u_light%dSpot", i + 1);
            m_pMat->SetUniform(buff, glm::vec4(spotDir, m_spotAngle));
        }

        m_pMat->SetUniform("u_ambientLight", glm::vec3(0.2f,0.2f,0.2f));
        m_pMat->SetUniform("u_specularColor", glm::vec3(1.0f,1.0f,1.0f));
        m_pMat->SetUniform("u_shininess", 200.0f);
        m_pMat->SetUniform("u_diffuseTex", 0);

        m_pOrbitCam = new OrbitCamera(m_pApp);
        m_pOrbitCam->focusOn(min,max);

        float gridSize = 2.5f * wolf::max(max.x,max.z);
        m_pGrid = new Grid3D(10, gridSize / 10.0f);
        m_pGrid->hideAxes();
    }

    printf("Successfully initialized Multi Spot Light Sample\n");
}

void SampleMultiSpotLights::update(float dt) 
{
    m_pOrbitCam->update(dt);
    m_pGrid->update(dt);

    m_timer += dt;

    float heightSinTheta = sin(m_timer / 4.0f);
    float sinTheta = sin(m_timer);
    float cosTheta = cos(m_timer);
    float height = 20.0f * ((1.0f + heightSinTheta) / 2.0f);

    // NOTE: Don't do per-frame string operations like this when possible - it's not efficient.
    //       I'm only doing it to keep the code concise for sample purposes
    char buff[64];
    for(int i = 0; i < 4; ++i)
    {
        float offsetTimer = m_timer + m_lightAnimOffsets[i];
        glm::vec3 lightTarget = m_modelCenter + glm::vec3(2.0f * cos(offsetTimer), 3.0f * sin(offsetTimer), 2.0f * cos(offsetTimer));

        glm::vec3 spotDir = glm::normalize(lightTarget - LIGHT_POSITIONS[i]);
        sprintf(buff, "u_light%dSpot", i + 1);
        m_pMat->SetUniform(buff, glm::vec4(spotDir, m_spotAngle));
    }

    if(m_pApp->isKeyDown(GLFW_KEY_UP))
    {
        m_spotAngle -= 1.0f;
        if(m_spotAngle < 5.0f)
            m_spotAngle = 5.0f;
    }

    if(m_pApp->isKeyDown(GLFW_KEY_DOWN))
    {
        m_spotAngle += 1.0f;
        if(m_spotAngle > 128.0f)
            m_spotAngle = 128.0f;
    }
}

void SampleMultiSpotLights::render(int width, int height)
{
	glClearColor(0.3f, 0.3f, 0.3f, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 mProj = m_pOrbitCam->getProjMatrix(width, height);
	glm::mat4 mView = m_pOrbitCam->getViewMatrix();
	glm::mat4 mWorld = glm::rotate(glm::mat4(1.0f), m_rot, glm::vec3(0.0f, 1.0f, 0.0f));

    m_pGrid->render(mView, mProj);

    m_pMat->SetUniform("u_viewPos", m_pOrbitCam->getViewPosition());

    m_pTex->Bind();
    m_pModel->Render(mWorld, mView, mProj);

    for(int i = 0; i < 4; ++i)
        m_pLightDbgSpheres[i]->Render(mWorld,mView,mProj);
}

