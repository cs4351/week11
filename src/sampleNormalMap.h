#pragma once
#include "../wolf/wolf.h"
#include "../samplefw/Sample.h"
#include "../samplefw/Grid3D.h"
#include "../samplefw/OrbitCamera.h"
#include "../samplefw/Sphere.h"

class SampleNormalMap: public Sample
{
public:
    SampleNormalMap(wolf::App* pApp) : Sample(pApp,"Normal Maps") {}
    ~SampleNormalMap();

    void init() override;
    void update(float dt) override;
    void render(int width, int height) override;

private:
    wolf::Model* m_pModel = nullptr;
    wolf::Material* m_pMat = nullptr;
    wolf::Texture* m_pTex = nullptr;
    wolf::Texture* m_pNormalMap = nullptr;
    Sphere* m_pLightDbg = nullptr;
    Grid3D* m_pGrid = nullptr;
    OrbitCamera* m_pOrbitCam = nullptr;
    float m_rot = 0.0f;
    float m_timer = 0.0f;
    float m_spotAngle = 10.0f;
    glm::vec3 m_modelCenter;
    glm::vec3 m_lightPos;
};
