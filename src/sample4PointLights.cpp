#include "sample4PointLights.h"

class SingleMaterialProvider : public wolf::Model::MaterialProvider
{
public:
    SingleMaterialProvider(const std::string& matName) : m_matName(matName) { }

    wolf::Material* getMaterial(const std::string& nodeName, int subMeshIndex, const std::string& name) const override
    {
        // Regardless of what mesh index or mat name the model wants, we just
        // use the mat we were seeded with. Note that we create a new one each
        // time so the DestroyMaterial calls line up. This could be improved,
        // but they do share shaders.
        return wolf::MaterialManager::CreateMaterial(m_matName);
    }

private:
    std::string m_matName;
};

Sample4PointLights::~Sample4PointLights()
{
	printf("Destroying 4 Point Lights Sample\n");
    if(m_pModel)
    {
        wolf::MaterialManager::DestroyMaterial(m_pMat);
        wolf::TextureManager::DestroyTexture(m_pTex);
        delete m_pModel;
        delete m_pGrid;
        delete m_pOrbitCam;
        for (int i = 0; i < NUM_LIGHTS; ++i)
            delete m_pLightDbgSpheres[i];
    }
}

void Sample4PointLights::init()
{
    m_lightColors[0] = glm::vec3(1.0f,0.0f,0.0f);
    m_lightColors[1] = glm::vec3(1.0f,1.0f,0.0f);
    m_lightColors[2] = glm::vec3(0.0f,1.0f,0.0f);
    m_lightColors[3] = glm::vec3(0.0f,0.0f,1.0f);

	// Only init if not already done
    if(!m_pModel)
    {
        m_pTex = wolf::TextureManager::CreateTexture("data/diffuseGray.png");
        m_pTex->SetWrapMode(wolf::Texture::WM_Repeat);

        const std::string MATNAME = "sample4PointLights";
        m_pMat = wolf::MaterialManager::CreateMaterial(MATNAME);
        m_pMat->SetProgram("data/fourPointLights.vsh", "data/fourPointLights.fsh");
        m_pMat->SetDepthTest(true);
        m_pMat->SetDepthWrite(true);

        char buff[64];
        for(int i = 0; i < NUM_LIGHTS; ++i)
        {
            m_pLightDbgSpheres[i] = new Sphere(0.25f);
            m_pLightDbgSpheres[i]->SetColor(m_lightColors[i]);

            sprintf(buff, "u_light%dColor", i + 1);
            m_pMat->SetUniform(buff, m_lightColors[i]);

            sprintf(buff, "u_light%dAttenuation", i + 1);
            m_pMat->SetUniform(buff, glm::vec3(0.0f,0.3f,0.0f));
        }

        m_pMat->SetUniform("u_ambientLight", glm::vec3(0.2f,0.2f,0.2f));
        m_pMat->SetUniform("u_specularColor", glm::vec3(1.0f,1.0f,1.0f));
        m_pMat->SetUniform("u_shininess", 200.0f);
        m_pMat->SetUniform("u_diffuseTex", 0);

        SingleMaterialProvider matProvider(MATNAME);
        m_pModel = new wolf::Model("data/unrealSmall.fbx", matProvider);

        glm::vec3 min = m_pModel->getAABBMin();
        glm::vec3 max = m_pModel->getAABBMax();
        glm::vec3 center = m_pModel->getAABBCenter();

        m_pOrbitCam = new OrbitCamera(m_pApp);
        m_pOrbitCam->focusOn(min,max);

        float gridSize = 2.5f * wolf::max(max.x,max.z);
        m_pGrid = new Grid3D(10, gridSize / 10.0f);
        m_pGrid->hideAxes();
    }

    printf("Successfully initialized 4 Point Lights Sample\n");
}

void Sample4PointLights::update(float dt) 
{
    m_pOrbitCam->update(dt);
    m_pGrid->update(dt);

    m_timer += dt;

    float step = (2 * PI) / NUM_LIGHTS;
    for(int i = 0; i < NUM_LIGHTS; ++i)
    {
        float sinTheta = sin(m_timer + step * i);
        float cosTheta = cos(m_timer + step * i);
        float height = 0.0f;
        
        if(i % 2 == 0)
            height = 20.0f * ((1.0f + sinTheta) / 2.0f);
        else 
            height = 20.0f - (20.0f * ((1.0f + sinTheta) / 2.0f));

        m_lightPositions[i] = glm::vec3(12.0f * cosTheta, height, 12.0f * sinTheta);
        m_pLightDbgSpheres[i]->SetPosition(m_lightPositions[i]);
    }
}

void Sample4PointLights::render(int width, int height)
{
	glClearColor(0.3f, 0.3f, 0.3f, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 mProj = m_pOrbitCam->getProjMatrix(width, height);
	glm::mat4 mView = m_pOrbitCam->getViewMatrix();
	glm::mat4 mWorld = glm::rotate(glm::mat4(1.0f), m_rot, glm::vec3(0.0f, 1.0f, 0.0f));

    m_pGrid->render(mView, mProj);

    m_pMat->SetUniform("u_viewPos", m_pOrbitCam->getViewPosition());

    m_pTex->Bind();
    m_pModel->Render(mWorld, mView, mProj);

    // NOTE: Don't do per-frame string operations like this when possible - it's not efficient.
    //       I'm only doing it to keep the code concise for sample purposes
    char buff[64];
    for(int i = 0; i < NUM_LIGHTS; ++i)
    {
        sprintf(buff, "u_light%dPosRange", i + 1);
        m_pMat->SetUniform(buff, glm::vec4(m_lightPositions[i], 100.0f));
        m_pLightDbgSpheres[i]->Render(mWorld,mView,mProj);
    }
}

