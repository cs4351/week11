#pragma once
#include "../wolf/wolf.h"
#include "../samplefw/Sample.h"
#include "../samplefw/Grid3D.h"
#include "../samplefw/OrbitCamera.h"
#include "../samplefw/Sphere.h"

class SampleMultiSpotLights: public Sample
{
public:
    SampleMultiSpotLights(wolf::App* pApp) : Sample(pApp,"Multiple Spot Lights") {}
    ~SampleMultiSpotLights();

    void init() override;
    void update(float dt) override;
    void render(int width, int height) override;

private:
    wolf::Model* m_pModel = nullptr;
    wolf::Material* m_pMat = nullptr;
    wolf::Texture* m_pTex = nullptr;
    Sphere* m_pLightDbgSpheres[4];
    Grid3D* m_pGrid = nullptr;
    OrbitCamera* m_pOrbitCam = nullptr;
    float m_rot = 0.0f;
    float m_timer = 0.0f;
    float m_spotAngle = 10.0f;
    glm::vec3 m_modelCenter;
    float m_lightAnimOffsets[4] = {0.2f,0.5f,0.8f,0.0f};
};
