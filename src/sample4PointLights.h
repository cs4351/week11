#pragma once
#include "../wolf/wolf.h"
#include "../samplefw/Sample.h"
#include "../samplefw/Grid3D.h"
#include "../samplefw/OrbitCamera.h"
#include "../samplefw/Sphere.h"

#define NUM_LIGHTS 4

class Sample4PointLights: public Sample
{
public:
    Sample4PointLights(wolf::App* pApp) : Sample(pApp,"4 Point Lights") {}
    ~Sample4PointLights();

    void init() override;
    void update(float dt) override;
    void render(int width, int height) override;

private:
    wolf::Model* m_pModel = nullptr;
    wolf::Material* m_pMat = nullptr;
    wolf::Texture* m_pTex = nullptr;
    Grid3D* m_pGrid = nullptr;
    OrbitCamera* m_pOrbitCam = nullptr;
    float m_rot = 0.0f;
    float m_timer = 0.0f;

    glm::vec3 m_lightPositions[NUM_LIGHTS];
    glm::vec3 m_lightColors[NUM_LIGHTS];
    Sphere* m_pLightDbgSpheres[NUM_LIGHTS];
};
