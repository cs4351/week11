#include <stdio.h>
#include <iostream>
#include <glm/glm.hpp>
#include "../wolf/wolf.h"
#include "../samplefw/SampleRunner.h"
#include "samplePointLight.h"
#include "sample2PointLights.h"
#include "sample4PointLights.h"
#include "sampleSpotLight.h"
#include "sampleMultiSpotLights.h"
#include "sampleNormalMap.h"
#include "sampleMultiPass.h"

class Week2: public wolf::App
{
public:
    Week2() : wolf::App("Week 4")
    {
        m_sampleRunner.addSample(new SamplePointLight(this));
        m_sampleRunner.addSample(new Sample2PointLights(this));
        m_sampleRunner.addSample(new Sample4PointLights(this));
        m_sampleRunner.addSample(new SampleSpotLight(this));
        m_sampleRunner.addSample(new SampleMultiSpotLights(this));
        m_sampleRunner.addSample(new SampleNormalMap(this));
        m_sampleRunner.addSample(new SampleMultiPass(this));
    }

    ~Week2()
    {
    }

    void update(float dt) override
    {
        if(isKeyDown(' '))
        {
            m_lastDown = true;
        }
        else if(m_lastDown)
        {
            m_sampleRunner.nextSample();
            m_lastDown = false;
        }

        m_sampleRunner.update(dt);
    }

    void render() override
    {
        m_sampleRunner.render(m_width, m_height);
    }

private:
    SampleRunner m_sampleRunner;
    bool m_lastDown = false;
};

int main(int, char**) {
    Week2 week2;
    week2.run();
}