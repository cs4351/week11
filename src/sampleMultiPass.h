#pragma once
#include "../wolf/wolf.h"
#include "../samplefw/Sample.h"
#include "../samplefw/Grid3D.h"
#include "../samplefw/OrbitCamera.h"
#include "../samplefw/Sphere.h"

#define NUM_LIGHTS_PER_PASS 4

class SampleMultiPass: public Sample
{
public:
    SampleMultiPass(wolf::App* pApp) : Sample(pApp,"Multi Pass Lights") {}
    ~SampleMultiPass();

    void init() override;
    void update(float dt) override;
    void render(int width, int height) override;

private:
    void _addLight();

    wolf::Model* m_pModel = nullptr;
    wolf::Material* m_pMatLights = nullptr;
    wolf::Material* m_pMatBase = nullptr;
    wolf::Texture* m_pTex = nullptr;
    wolf::Texture* m_pNormalMap = nullptr;
    Grid3D* m_pGrid = nullptr;
    OrbitCamera* m_pOrbitCam = nullptr;
    float m_rot = 0.0f;
    float m_timer = 0.0f;
    bool m_keyDown = false;

    struct Light
    {
        glm::vec3 position;
        glm::vec3 color;
        glm::vec3 dest;
        Sphere* pDbgSphere = nullptr;
    };

    std::vector<Light> m_lights;
};
