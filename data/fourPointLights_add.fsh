// Light 1
uniform vec4 u_light1PosRange; // range is in .w
uniform vec3 u_light1Color;
uniform vec3 u_light1Attenuation;

// Light 2
uniform vec4 u_light2PosRange; // range is in .w
uniform vec3 u_light2Color;
uniform vec3 u_light2Attenuation;

// Light 3
uniform vec4 u_light3PosRange; // range is in .w
uniform vec3 u_light3Color;
uniform vec3 u_light3Attenuation;

// Light 4
uniform vec4 u_light4PosRange; // range is in .w
uniform vec3 u_light4Color;
uniform vec3 u_light4Attenuation;

// Material properties
uniform sampler2D u_diffuseTex;
uniform sampler2D u_normalMap;
uniform vec3 u_specularColor;
uniform float u_shininess;

// Scene properties
uniform vec3 u_viewPos;
uniform vec3 u_ambientLight;

in vec4 v_normal;
in vec2 v_uv1;
in vec3 v_pos;
in vec4 v_tangent;
out vec4 PixelColor;

// NOTE: A lot of this work could be done in the vertex shader. Keeping it simple here, but it's
//       less efficient
vec3 tangent_to_world_space(vec3 normalMapSample, vec3 normal, vec3 tangent)
{
	vec3 nT = 2.0 * normalMapSample - 1.0;

	vec3 N = normal;
	vec3 T = normalize( tangent - dot(tangent, N) * N);
	vec3 B = cross(N, T);

	mat3 TBN = mat3(T,B,N);

	vec3 bumpNormal = TBN * nT;
	return bumpNormal;
}

vec3 calc_lighting(vec4 lightPosRange, vec3 lightColor, vec3 lightAttenuation, vec3 samplePos, vec3 n, vec3 viewDir, vec3 diffuseColor, vec3 specularColor, float shininess)
{
	vec3 lightDir = lightPosRange.xyz - samplePos.xyz;
	float distance = length(lightDir);

	if( distance > lightPosRange.w )
	{
		return vec3(0,0,0);	
	}
	else
	{
		// normalize it
		lightDir /= distance;
		// Diffuse
		vec3 diffuse = diffuseColor * lightColor * max(0.0,dot(n, lightDir));
		// Specular
		vec3 R = reflect(-lightDir,n);
		vec3 spec = pow(max(dot(R, viewDir), 0.0), shininess) * lightColor * specularColor;
		// Combine the components
		vec3 light = clamp(diffuse + spec, 0, 1);
		// Attenuate the light
		float att = 1.0 / dot(lightAttenuation, vec3(1.0, distance, distance*distance));
		light *= att;

		return light;
	}
}

void main()
{
    vec3 diffuseMat = texture(u_diffuseTex, v_uv1).rgb;
	vec3 faceNormal = normalize( v_normal.xyz );
	vec3 normalMapSample = texture(u_normalMap, v_uv1 * 5.0).rgb;
	vec3 n = tangent_to_world_space( normalMapSample, faceNormal, v_tangent.xyz );

	vec3 color = vec3(0,0,0);

	vec3 viewDir = normalize(u_viewPos - v_pos);
	color += calc_lighting(u_light1PosRange, u_light1Color, u_light1Attenuation, v_pos, n, viewDir, diffuseMat, u_specularColor, u_shininess);
	color += calc_lighting(u_light2PosRange, u_light2Color, u_light2Attenuation, v_pos, n, viewDir, diffuseMat, u_specularColor, u_shininess);
	color += calc_lighting(u_light3PosRange, u_light3Color, u_light3Attenuation, v_pos, n, viewDir, diffuseMat, u_specularColor, u_shininess);
	color += calc_lighting(u_light4PosRange, u_light4Color, u_light4Attenuation, v_pos, n, viewDir, diffuseMat, u_specularColor, u_shininess);

	PixelColor = vec4(color, 1.0);
}
