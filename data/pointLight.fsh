// Light properties
uniform vec3 u_lightPos;
uniform float u_lightRange;
uniform vec3 u_lightColor;
uniform vec3 u_lightAttenuation;

// Scene properties
uniform vec3 u_ambientLight;
uniform vec3 u_viewPos;

// Material properties
uniform vec3 u_specularColor;
uniform float u_shininess;
uniform sampler2D u_diffuseTex;

// Varying inputs
in vec4 v_normal;
in vec2 v_uv1;
in vec3 v_pos;

// Output
out vec4 PixelColor;

void main()
{
    vec3 diffuseMat = texture(u_diffuseTex, v_uv1).rgb;

	vec3 n = normalize( v_normal.xyz );
	vec3 lightDir = u_lightPos - v_pos.xyz;
	float distance = length(lightDir);

	if( distance > u_lightRange )
	{
		PixelColor = vec4(diffuseMat * u_ambientLight, 1.0);
	}
	else
	{
		vec3 viewDir = normalize(u_viewPos - v_pos);

		lightDir /= distance; // normalize it
		// Ambient
		vec3 ambient = diffuseMat * u_ambientLight;
		// Diffuse
		vec3 diffuse = diffuseMat * u_lightColor * max(0.0,dot(n, lightDir));
		// Specular
		vec3 R = reflect(-lightDir,n);
		vec3 spec = pow(max(dot(R, viewDir), 0.0), u_shininess) * u_lightColor * u_specularColor;
		// Combine the components
		vec3 light = clamp(diffuse + spec, 0, 1);
		// Attenuate the light
		float att = 1.0 / dot(u_lightAttenuation, vec3(1.0, distance, distance*distance));
		light *= att;

		PixelColor = vec4(ambient + light.rgb, 1.0);
	}
}

