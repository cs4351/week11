
uniform vec3 u_ambientLight;
uniform sampler2D u_diffuseTex;

in vec2 v_uv1;
out vec4 PixelColor;

void main()
{
    vec3 diffuseMat = texture(u_diffuseTex, v_uv1).rgb;
	vec3 color = u_ambientLight * diffuseMat;
	PixelColor = vec4(color, 1.0);
}

